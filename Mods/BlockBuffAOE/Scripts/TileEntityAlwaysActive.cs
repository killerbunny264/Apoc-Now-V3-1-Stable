using System.IO;
using UnityEngine;
using Platform;

public class TileEntityAlwaysActive : TileEntity
{


    public TileEntityAlwaysActive(Chunk _chunk) :
        base(_chunk)
    {}
    public override TileEntityType GetTileEntityType()
    {
        return (TileEntityType) 244;
    }
    public override bool IsActive(World world)
    {
        return true;
    }

}
