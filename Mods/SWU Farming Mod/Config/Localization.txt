﻿Key,File,Type,UsedInMainMenu,NoTranslate,english,Context / Alternate Text,german,latam,french,italian,japanese,koreana,polish,brazilian,russian,turkish,schinese,tchinese,spanish
SWUstrawberrieSeeds,blocks,block,,,Strawberry seeds,,Erdbeersamen,,,,,,,,,,,,
SWUStrawberrie2,blocks,block,,,Growing strawberry plants,,wachsender Erdbeerpflanzen,,,,,,,,,,,,
SWUStrawberrie3,blocks,block,,,Harvestable strawberry plants,,Erntbar Erdbeerpflanzen,,,,,,,,,,,,
SWUStrawberrie3Harvest,blocks,block,,,strawberry plant,,Erdbeerpflanzen,,,,,,,,,,,,
SWUcabbageSeeds,blocks,block,,,Cabbage seeds,,Kohlsamen,,,,,,,,,,,,
SWUCabbage2,blocks,block,,,Growing Cabbage plants,,wachsende Kohlpflanzen,,,,,,,,,,,,
SWUCabbage3,blocks,block,,,Harvestable Cabbage plants,,Erntbare Kohlpflanzen,,,,,,,,,,,,
SWUCabbage3Harvest,blocks,block,,,Cabbage plants,,Kohlpflanzen,,,,,,,,,,,,
SWUcarrotSeeds,blocks,block,,,Carrot seeds,,Karottensamen,,,,,,,,,,,,
SWUCarrot2,blocks,block,,,Growing Carrot plant,,Karottenpflanzen anbauen,,,,,,,,,,,,
SWUCarrot3,blocks,block,,,Harvestable Carrot plants,,Erntbare Karottenpflanzen,,,,,,,,,,,,
SWUCarrot3Harvest,blocks,block,,,Carrot plant,,Karottenpflanzen,,,,,,,,,,,,
SWUcucumberSeeds,blocks,block,,,Cucumber seeds,,Gurkensamen,,,,,,,,,,,,
SWUCucumber2,blocks,block,,,Growing Cucumber plants,,Wachsende Gurkenpflanzen,,,,,,,,,,,,
SWUCucumber3,blocks,block,,,Harvestable Cucumber plants,,Erntbar Gurkenpflanzen,,,,,,,,,,,,
SWUCucumber3Harvest,blocks,block,,,Cucumber plant,,Gurkenpflanzen,,,,,,,,,,,,
SWUtomatoSeeds,blocks,block,,,Tomato seeds,,Tomatensamen,,,,,,,,,,,,
SWUTomato2,blocks,block,,,Growing Tomato plants,,Wachsende Tomatenpflanzen,,,,,,,,,,,,
SWUTomato3,blocks,block,,,Harvestable Tomato plants,,Erntbare Tomatenpflanzen,,,,,,,,,,,,
SWUTomato3Harvest,blocks,block,,,Tomato plant,,Tomatenpflanzen,,,,,,,,,,,,
SWUBananaTreeHarvest,blocks,block,,,"Banana tree",,"Bananenbaum",,,,,,,,,,,,
SWUCoconutTreeHarvest,blocks,block,,,"Coconut tree",,"Kokosnussbaum",,,,,,,,,,,,
SWUBananaTreeGrowing,blocks,block,,,"Growing Banana tree",,"Bananenbaum wächst",,,,,,,,,,,,
SWUCoconutTreeGrowing,blocks,block,,,"Growing Coconut tree",,"Kokosnussbaum wächst",,,,,,,,,,,,
SWUBananaTreeHarvestDesc,blocks,block,,,"Banana tree",,"Bananenbaum",,,,,,,,,,,,
SWUCoconutTreeHarvestDesc,blocks,block,,,"Coconut tree",,"Kokosnussbaum",,,,,,,,,,,,
SWUAppleTreeSeed,blocks,block,,,Apple seeds,,Apfelsamen,,,,,,,,,,,,
SWUAppleTreeGrowing,blocks,block,,,Growing Apple tree,,wachsender Apfelbaum,,,,,,,,,,,,
SWUAppleTreeHarvest,blocks,block,,,Apple tree,,Apfelbaum,,,,,,,,,,,,
SWUCherryTreeSeed,blocks,block,,,Cherry seeds,,Kirschsamen,,,,,,,,,,,,
SWUCherryTreeGrowing,blocks,block,,,Growing Cherry tree,,wachsender Kirschbaum,,,,,,,,,,,,
SWUCherryTreeHarvest,blocks,block,,,Cherry tree,,Kirschbaum,,,,,,,,,,,,
SWUBananaTreeSeed,blocks,block,,,Banana seeds,,Bananensamen,,,,,,,,,,,,
SWUCoconutTreeSeed,blocks,block,,,Coconut seeds,,KokosnussSamen,,,,,,,,,,,,










